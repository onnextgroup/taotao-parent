package com.taotao.service;

/**
 * 商品规格参数
 */
public interface ItemParamItemService {

    String getItemParamByItemId(Long itemId);
}
